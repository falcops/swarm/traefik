production:
	printf "${CF_DNS_API_TOKEN}" | sudo docker secret create cf_dns_api_token_${DEPLOYMENT_ID} - 
	sudo -E docker stack deploy --prune -c docker-compose.yml traefik

test:
	sudo docker stack deploy --prune -c docker-compose.test.yml test
